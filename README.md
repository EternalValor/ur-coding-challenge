This project was created as a coding challenge for the second step in the United Remote application process.

# How to run it:

First clone the repository and run `yarn install` or `npm install` in the project folder to install dependencies.
Then run `yarn start` or `npm start` in the project folder to start the development server. Then navigate to localhost:3000 in your browser to view the coding challenge. When the project is loaded, please click the **Load Repos** button to start.

# Choice of libraries:

## React.js

I chose React.js as it is lightweight, easy to use, and it's the library I've gathered the most experience on.

## Webfontloader

Used to load google web fonts.

# How endless scrolling was acheived:

I implemented a scroll event listener on the window object and tested whether a new api call should be performed. Although it isn't as optimal of a solution as intersectionObserver would be, I chose to use it because of its browser support.

# Potentional Issue:

Not all repos are 30 days old or less, so multiple api calls must be performed before any scrolling can be done on the page.
