import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import WebFont from 'webfontloader';
import './main.scss';

// Used to load online fonts
WebFont.load({
  google: {
    families: ['Roboto:300,400', 'sans-serif']
  }
});

ReactDOM.render(<App />, document.getElementById('root'));
