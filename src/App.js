import React from 'react';
import Repo from './Components/Repo';
import Loading from './Components/Loading';
import mapApiToModel from './helpers/mapApiToModel';
import Repository, { apiToRepoMap } from './models/Repository';

class App extends React.Component {
  state = {
    display: false,
    repos: [],
    page: 0,
    repoIds: []
  };

  loadRepos = () => {
    const url = `https://api.github.com/search/repositories?q=created:>2017-10-22&sort=stars&order=desc&page=${
      this.state.page
    }`;
    fetch(url, {
      headers: {
        'User-Agent': 'request'
      }
    })
      .then(response => response.json())
      .then(data => {
        const mappedData = data.items.reduce((newData, dataEl) => {
          newData.push(mapApiToModel(dataEl, apiToRepoMap, Repository));
          return newData;
        }, []);

        // Keep track of which repos have been added to DOM
        let repoIds = [...this.state.repoIds];
        mappedData.forEach(repo => {
          if (
            repo.age <= 30 &&
            repo.age >= 0 &&
            repoIds.indexOf(repo.id) === -1
          )
            repoIds.push(repo.id);
        });
        // Check if age is less than 30 and positive and repo not in state
        const newRepos = mappedData.filter(
          repo => repoIds.indexOf(repo.id) !== -1
        );

        const repos = [...this.state.repos, ...newRepos];
        let uniqueRepos = [];
        repos.forEach(repo => {
          let unique = true;
          uniqueRepos.forEach(uRepo => {
            if (uRepo.id === repo.id) unique = false;
          });
          if (unique) uniqueRepos.push(repo);
        });
        repos.filter((repo, index) => repos.indexOf(repo) === index);
        // If repo count didn't augment -> load more repos
        if (uniqueRepos.length === this.state.repos.length) this.loadMore();
        this.setState(
          {
            repos: uniqueRepos,
            repoIds,
            loading: false
          },
          () => {
            // Make sure that there are enough repos in the beginning to be able to scroll
            if (this.state.repos.length < 6) this.loadMore();
          }
        );
      })
      .catch(err => {
        if (this.state.repos.length === 0) this.loadMore();
      });
  };

  displayRepos = () => {
    this.setState({ displaying: true }, this.loadRepos);
  };

  loadMore = () => {
    // Limit amount of pages that can be viewed
    if (this.state.page < 10) {
      this.setState(
        prevState => ({ page: prevState.page + 1, loading: true }),
        this.loadRepos
      );
    }
  };

  shouldLoadMore = () => {
    const repos = document.querySelectorAll('.repo');
    if (repos.length) {
      // Last repo position
      const lastRepo = repos[repos.length - 1].getBoundingClientRect().y;

      // Check if the last repo is visibile to trigger loadMore
      if (lastRepo <= window.innerHeight && !this.state.loading) {
        this.loadMore();
      }
    }
  };

  componentDidMount() {
    let lastScrollTop = 0;
    window.addEventListener('scroll', e => {
      // Prevent loading more if scrolling up
      if (lastScrollTop < window.pageYOffset) {
        lastScrollTop = window.pageYOffset;
        this.shouldLoadMore();
      }
    });
  }

  componentDidUpdate() {}

  render() {
    const header = (
      <header className="header">
        <h1 className="heading">
          Most starred Github repos created in the last 30 days
        </h1>
        <h3 className="sub-heading">
          Created as a coding challenge for the United Remote application
          process
        </h3>
      </header>
    );

    if (this.state.displaying) {
      // Check whether or not load more has been clicked
      if (!this.state.repos.length) {
        // If repos aren't loaded, display a loader
        return (
          <div className="container">
            {header}
            <Loading />
          </div>
        );
      } else {
        return (
          <div className="container">
            {header}
            <div className="repos">
              {this.state.repos.map(repo => (
                <Repo key={repo.id} {...repo} />
              ))}
            </div>
            {this.state.page >= 10 ? null : <Loading />}
          </div>
        );
      }
    } else {
      // Display Load More button to let user list repos
      return (
        <div className="container">
          {header}
          <div className="btn" onClick={this.displayRepos}>
            Load Repos
          </div>
        </div>
      );
    }
  }
}

export default App;
