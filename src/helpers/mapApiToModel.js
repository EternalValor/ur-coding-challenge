/**
 *
 * @param {object} sourceObj - API result Object
 * @param {array} mapping - Mapping between API object and desired object
 */
const mapReducer = (sourceObj, mapping) => {
  return mapping.reduce((targetObj, mapEl) => {
    if (typeof mapEl[1] === 'string') {
      if (mapEl[0].includes('.'))
        targetObj[mapEl[1]] = getNested(sourceObj, mapEl[0]);
      else targetObj[mapEl[1]] = sourceObj[mapEl[0]];
    } else {
      const result = mapEl[1](sourceObj);
      Object.assign(targetObj, result);
    }
    return targetObj;
  }, {});
};

/**
 *
 * @param {object} apiObj - API Object
 * @param {array} modelMap - Mapping between API object and desired object
 * @param {function} modelPrototype - Prototype of model
 */
export default function(apiObj, modelMap, modelPrototype) {
  const data = mapReducer(apiObj, modelMap);
  return new modelPrototype(data);
}

// Function to get nested objects using string
function getNested(theObject, path, separator) {
  try {
    separator = separator || '.';

    return path
      .replace('[', separator)
      .replace(']', '')
      .split(separator)
      .reduce(function(obj, property) {
        return obj[property];
      }, theObject);
  } catch (err) {
    return undefined;
  }
}
