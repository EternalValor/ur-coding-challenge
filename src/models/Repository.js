// Repository Model
export default function Repository(data) {
  this.id = data && data.id ? data.id : 0;
  this.title = data && data.title ? data.title : '';
  this.imgUrl = data && data.imgUrl ? data.imgUrl : '';
  this.desc = data && data.desc ? data.desc : '';
  this.stars = data && data.stars ? data.stars : '';
  this.issues = data && data.issues ? data.issues : '';
  this.age = data && data.age ? data.age : '';
  this.owner = data && data.owner ? data.owner : '';
  this.url = data && data.url ? data.url : '';
}

// Mapping between Api object keys and repo object keys
export const apiToRepoMap = [
  ['id', 'id'],
  ['name', 'title'],
  ['owner.avatar_url', 'imgUrl'],
  ['description', 'desc'],
  ['stargazers_count', 'stars'],
  ['open_issues_count', 'issues'],
  [
    'created_at',
    apiObj => {
      const d = new Date('2017/11/22');
      const c = new Date(apiObj.created_at);
      const age = Math.floor(
        (Date.UTC(c.getFullYear(), c.getMonth(), c.getDate()) -
          Date.UTC(d.getFullYear(), d.getMonth(), d.getDate())) /
          (1000 * 60 * 60 * 24)
      );
      return { age };
    }
  ],
  ['owner.login', 'owner'],
  ['html_url', 'url']
];
