import React from 'react';

const loading = () => (
  <div className="lds-ripple">
    <div />
    <div />
  </div>
);

export default loading;
