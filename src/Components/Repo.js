import React from 'react';
import Icon from '../Components/Icon';

const repo = props => (
  <div className="repo">
    <div className="repo__avatar">
      <img src={props.imgUrl} alt="avatar" className="repo__avatar__img" />
    </div>
    <div className="repo__info">
      <div className="repo__title">
        <a href={props.url} className="link">
          {props.title}
        </a>
      </div>
      <div className="repo__description">{props.desc}</div>
      <div className="repo__facts">
        <span className="repo__fact">
          <Icon name="star-full" className="star-icon" />
          {props.stars}
        </span>
        <span className="repo__fact">Issues: {props.issues}</span>
        <span className="repo__fact">
          Submitted {props.age} days ago by {props.owner}
        </span>
      </div>
    </div>
  </div>
);

export default repo;
